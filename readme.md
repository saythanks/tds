# TDS Integration For Laravel

Provides API support for TDS ([Tops Digital Services](https://toppsdigitalservices.com/))

## About

Provides support for authenticaition as well as:
* Product Creation
* Cancel Product Creation 
* Reverse Cancel Product Creation
* Cancel Product
* Get Status


## Installation

Add the repository to your composer respository section:
```json
"repositories": [
   {
      "type": "vcs",
      "url": "git@bitbucket.org:saythanks/tds.git"
   }
],
```

Require the package via composer:
```sh
composer require saythanks/tds-api
```

## Configuration

Add your environment variables for the Host/Key/Secret
```
TDS_HOST=
TDS_API_KEY=
TDS_API_SECRET=
```

The defaults are set in `config/tds.php`. Publish the config to copy the file to your own config:
```sh
php artisan vendor:publish --provider="SayHi\Tds\TdsServiceProvider"
```




## Usage

Create a Product
```php
 $service = resolve(SayHi\Tds\Tds::class);
 $product = $service->createProduct($sku, $amount, $retailerName);
 $reversal = $service->reverseCreateProduct($retailerName, $sku, $amount, $product->object()->providerTransactionId);
```

Create and reverse a Product
```php
 $service = resolve(SayHi\Tds\Tds::class);
 $product = $service->createProduct($sku, $amount, $retailerName);
 $reversal = $service->reverseCreateProduct($retailerName, $sku, $amount, $product->object()->providerTransactionId);
```

Create and cancel Product
```php
 $service = resolve(SayHi\Tds\Tds::class);
 $product = $service->createProduct($sku, $amount, $retailerName);
 $reversal = $service->cancelProduct($retailerName, $product->object()->pin);
```

Get Status of a product
```php
 $service = resolve(SayHi\Tds\Tds::class);
 $status = $service->getStatus($pin);
```
