<?php

namespace SayHi\Tds;

use Illuminate\Http\Client\PendingRequest;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use SayHi\Tds\Exceptions\GenericException;
use SayHi\Tds\Exceptions\InvalidConfigException;
use SayHi\Tds\Exceptions\ValidationException;

/**
 * Class Tds
 * @package SayHi\Tds
 */
class Tds
{

    public string $host;
    private string $key;
    private string $secret;

    public const RESPONSE_SUCCESS = 0;
    public const RESPONSE_UNSPECIFIED = 1000;
    public const RESPONSE_INVALID_SIGNATURE = 1001;
    public const RESPONSE_INVALID_API_KEY = 1003;
    public const RESPONSE_INVALID_PIN_NUMBER = 1004;
    public const RESPONSE_SYSTEM_ERROR = 1005;
    public const RESPONSE_ALREADY_REDEEMED = 1009;
    public const RESPONSE_VALIDATION_FAILURE = 1010;
    public const RESPONSE_INVALID_RIGHTS = 1014;
    public const RESPONSE_PRODUCT_CANCELLED = 1015;
    public const RESPONSE_PRODUCT_EXPIRED = 1016;
    public const RESPONSE_PRODUCT_ACTIVE = 1017;
    public const RESPONSE_INVALID_SERVICE_PROVIDER_TRANSACTION_ID = 1018;
    public const RESPONSE_INVALID_PRODUCT_SKU = 1019;
    public const RESPONSE_INVALID_PRODUCT_DENOMINATION = 1020;
    public const RESPONSE_INVALID_PRODUCT_CURRENCY = 1021;

    /**
     * Tds constructor.
     * @throws InvalidConfigException
     */
    public function __construct()
    {
        $this->host = config('tds.host');
        $this->key = config('tds.key');
        $this->secret = config('tds.secret');

        $this->validateConfig();
    }

    public function getStatus(?string $pin = null, ?string $serialNumber = null)
    {
        if(is_null($serialNumber) && is_null($pin))
        {
            throw new ValidationException('Either product or pin is required to GetStatus');
        }
        $url = '/v2/prepaid/getstatus';
        $data = [
            'apiKey' => $this->key,
            'product' => $serialNumber,
            'pin' => $pin,
            'ts' => time(),
        ];

        $response = $this->post($url, $data);
        return $response;
    }

    public function createProduct(?string $retailer, ?string $sku, ?string $amount)
    {
        $url = '/product/create';
        $data = [
            'sku' => $sku,
            'providerTransactionId' => $this->generateProviderTransactionId(),
            'productDenomination' => $amount,
            'productCurrency' => 'ZAR',
            'merchant' => $retailer,
            'ts' => time(),
            'addlProductInfo' => 'true',
            'marketingCode' => null,
            'merchantLocation' => null,
            'merchantAddress' => null,
            'merchantCity' => null,
            'merchantState' => null,
            'merchantZip' => null,
            'merchantCountry' => null,
            'merchantTerminalId' => null,
            'merchantStoreId' => null,
            'providerLabel' => null,
            'providerLocation' => null,
            'providerUserId' => null,
        ];
        
        $response = $this->post($url, $data);
        $resultCode = $response->object()->resultCode;
        if($resultCode === self::RESPONSE_SUCCESS)
        {
            Log::info('TDS-API: Created TDS product', [
                'pin' => $response->object()->pin,
                'providerTransactionId' => $response->object()->providerTransactionId,
                'serialNumber' => $response->object()->serialNumber
            ]);
            return $response;
        }
        Log::error('TDS-API: Unexpected Response', ['statusCode' => $resultCode, 'data' => $response->json()]);
        throw new GenericException('Failed to create product');

    }

    public function reverseCreateProduct(?string $retailer, ?string $sku, ?string $amount, ?string $providerTransactionId)
    {
        $url = '/product/create/reversal';
        $data = [
            'sku' => $sku,
            'providerTransactionId' => $providerTransactionId,
            'productDenomination' => $amount,
            'productCurrency' => 'ZAR',
            'merchant' => $retailer,
            'ts' => time(),
            'marketingCode' => null,
            'merchantLocation' => null,
            'merchantAddress' => null,
            'merchantCity' => null,
            'merchantState' => null,
            'merchantZip' => null,
            'merchantCountry' => null,
            'merchantTerminalId' => null,
            'merchantStoreId' => null,
            'providerLabel' => null,
            'providerLocation' => null,
            'providerUserId' => null,
        ];

        $response = $this->post($url, $data);
        $resultCode = $response->object()->resultCode;
        if($resultCode === self::RESPONSE_SUCCESS)
        {
            Log::info('TDS-API: Reversed Product Creation', ['providerTransactionId' => $providerTransactionId]);
            return $response;
        }
        Log::error('TDS-API: Unexpected Response', ['statusCode' => $resultCode, 'data' => $response->json()]);
        throw new GenericException('Failed to reverse product creation');

    }

    public function cancelProduct(?string $retailer, ?string $pin = null, ?string $serialNumber = null)
    {
        if(!$pin && !$serialNumber)
        {
            throw new ValidationException('Either Pin or Serial Number is required to cancel a product');
        }
        $url = '/product/cancel';
        $data = [
            'pin' => $pin,
            'serialNumber' => $serialNumber,
            'providerTransactionId' => $this->generateProviderTransactionId(),
            'merchant' => $retailer,
            'ts' => time(),
            'marketingCode' => null,
            'merchantLocation' => null,
            'merchantAddress' => null,
            'merchantCity' => null,
            'merchantState' => null,
            'merchantZip' => null,
            'merchantCountry' => null,
            'merchantTerminalId' => null,
            'merchantStoreId' => null,
            'providerLabel' => null,
            'providerLocation' => null,
            'providerUserId' => null,
        ];

        $response = $this->post($url, $data);
        $resultCode = $response->object()->resultCode;
        if($resultCode === self::RESPONSE_SUCCESS)
        {
            Log::info('TDS-API: Cancelled TDS product', ['pin' => $pin, 'providerTransactionId' => $response->object()->providerTransactionId,]);
            return $response;
        }
        else if($resultCode === self::RESPONSE_PRODUCT_CANCELLED)
        {
            Log::warning("Tried to cancel a product which was already cancelled", ['response' => $response->json()]);
            return $response;
        }
        Log::error('TDS-API: Unexpected Response', ['statusCode' => $resultCode, 'data' => $response->json()]);
        throw new GenericException("Failed to cancel product");
    }

    public function reverseCancelProduct(?string $retailer, string $providerTransactionId, $pin = null, $serialNumber = null)
    {
        if(!$pin && !$serialNumber)
        {
            throw new ValidationException('Either Pin or Serial Number is required to reverse a product cancellation');
        }
        $url = '/product/cancel/reversal';
        $data = [
            'pin' => $pin,
            'serialNumber' => $serialNumber,
            'providerTransactionId' => $providerTransactionId,
            'merchant' => $retailer,
            'ts' => time(),
            'marketingCode' => null,
            'merchantLocation' => null,
            'merchantAddress' => null,
            'merchantCity' => null,
            'merchantState' => null,
            'merchantZip' => null,
            'merchantCountry' => null,
            'merchantTerminalId' => null,
            'merchantStoreId' => null,
            'providerLabel' => null,
            'providerLocation' => null,
            'providerUserId' => null,
        ];

        $response = $this->post($url, $data);
        $resultCode = $response->object()->resultCode;
        if($resultCode === self::RESPONSE_SUCCESS)
        {
            Log::info('TDS-API: Reversed Product Cancellation', ['providerTransactionId' => $providerTransactionId]);
            return $response;
        }
        Log::error('TDS-API: Unexpected Response', ['statusCode' => $resultCode, 'data' => $response->json()]);
        throw new GenericException('Failed to reverse product cancellation');

    }

    /**
     * @throws InvalidConfigException
     */
    protected function validateConfig(): void
    {
        $this->validateConfigHost();
    }

    /**
     * @throws InvalidConfigException
     */
    protected function validateConfigHost()
    {
        if(!$this->host)
        {
            throw new InvalidConfigException("Unable to find TDS Host in your config!");
        }
    }


    /**
     * @return PendingRequest
     */
    public function tdsApi(){
        return Http::retry(3, 100)
            ->asForm()
            ->acceptJson();
    }

    protected function post($urlPart, $data)
    {
        Log::info('TDS-API: Sending post', ['url' => $urlPart, 'data' => $data]);
        $this->appendSignature($urlPart, $data);
        $response = $this->tdsApi()->post($this->host . $urlPart, $data);
        Log::debug('TDS-API: Got response from post', ['url' => $urlPart, 'data' => $data, 'response-json' => $response->json()]);

        return $response;
    }

    protected function appendSignature($url, &$data){
        $data['apiKey'] = $this->key;

        ksort($data);
        
        $generatedString = $url;
        foreach ($data as $key => $value){
            if(is_null($value)){
                unset($data[$key]);
                continue;
            }
            $generatedString .= $key . '=' . $value;
        }
        $generatedString .= $this->secret;
        $data['sig'] = sha1($generatedString);
    }

    protected function generateProviderTransactionId() : string
    {
        return (string) microtime(true);
    }

}
