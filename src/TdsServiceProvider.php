<?php

namespace SayHi\Tds;

use Illuminate\Support\ServiceProvider;

class TdsServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/config/tds.php', 'tds');
    }

    public function boot()
    {
        $this->publishes(
            [
                __DIR__ . '/config/tds.php' => config_path('tds.php'),
            ], 'config');
    }


}