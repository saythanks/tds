<?php

return

    /*
    |--------------------------------------------------------------------------
    | TDS API Settings
    |--------------------------------------------------------------------------
    |
    |
    */
    [
        'host' => env('TDS_HOST', 'https://stage.api.gmgpulse.com'),
        'key' => env('TDS_API_KEY'),
        'secret' => env('TDS_API_SECRET'),


    ];